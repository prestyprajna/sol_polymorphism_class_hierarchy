﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sol_Polymorphism_Class_Hierarchy
{
    class Program
    {
        static void Main(string[] args)
        {
            Shape shapeObj = new Shape();  //tight -couple
            shapeObj.Draw();

            Shape shapeObj1 = new Circle();  //loose-couple (dynamic polymorphism)
            shapeObj1.Draw();

            Shape shapeObj2 = new Triangle();  //upcasting (implicit conversion)
            shapeObj2.Draw();

            Circle circleObj = (Circle)shapeObj1;  //downcastiong (explicit conversion)
            //Circle circleObj = shapeObj1 as Circle;  
            circleObj?.Draw();
        }
    }
    
    public class Shape
    {
        public virtual void Draw()
        {
            Console.WriteLine("shape-draw");
        }
    }

    public class Circle : Shape
    {
        public override void Draw()
        {
            //base.Draw();
            Console.WriteLine("circle-draw");
        }
    }

    public class Triangle : Shape
    {
        public override void Draw()
        {
            //base.Draw();
            Console.WriteLine("triangle-draw");
        }
    }
}
